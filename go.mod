module github.com/dewadg/twtx

go 1.14

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/render v1.0.1
	github.com/graphql-go/graphql v0.7.9
	github.com/joho/godotenv v1.3.0
)
